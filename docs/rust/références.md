#### Référence

>[C, C++, Rust, Python, and Carbon (When to use Which?)](https://medium.com/codex/c-c-rust-python-and-carbon-when-to-use-which-2912a88f205b)

>[Rust Tools Install](https://www.rust-lang.org/tools/install)

>[Visual C++ Build Tools](https://visualstudio.microsoft.com/fr/visual-cpp-build-tools/)

>[Rust site officiel](https://www.rust-lang.org/fr)

>[Crates.io](https://crates.io/crates)


