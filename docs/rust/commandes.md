
-   Nous pouvons créer un projet en utilisant `cargo new`.
-   Nous pouvons compiler un projet en utilisant `cargo build`.
-   Nous pouvons compiler puis exécuter un projet en une seule fois en utilisant `cargo run`.
-   Nous pouvons compiler un projet sans produire de binaire afin de vérifier l'existance d'erreurs en utilisant `cargo check`.
-   Au lieu d'enregistrer le résultat de la compilation dans le même dossier que votre code, Cargo l'enregistre dans le dossier _target/debug_.
- `cargo build --release` pour optimiser le code de release

```bash
$ git clone example.org/projet_quelconque
$ cd projet_quelconque
$ cargo build
```

- Documentation | ``cargo doc --open``

