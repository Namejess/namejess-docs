```rust
use std::io;

fn main() {

    println!("Bienvenue dans le jeu du plus ou du moins !");

    println!("Veuillez entrer un nombre entre 1 et 100 :");

  

    let mut nombre_a_trouver = String::new();

  

    std::io::stdin()

        .read_line(&mut nombre_a_trouver)

        .expect("Erreur de lecture");

  

    println!("Vous avez entré : {}", nombre_a_trouver);

}
```

Ce code contient beaucoup d'informations, nous allons donc l'analyser petit à petit. Pour obtenir la saisie utilisateur et ensuite l'afficher, nous avons besoin d'importer la bibliothèque d'entrée/sortie `io` (initiales de _input/output_) afin de pouvoir l'utiliser. La bibliothèque `io` provient de la bibliothèque standard, connue sous le nom de `std` :

```rust
use std::io;
```

Par défaut, Rust importe dans la portée de tous les programmes quelques fonctionnalités définies dans la bibliothèque standard. Cela s'appelle _l'étape préliminaire (the prelude)_, et vous pouvez en savoir plus dans sa [documentation de la bibliothèque standard](https://doc.rust-lang.org/std/prelude/index.html).

Si vous voulez utiliser un type qui ne s'y trouve pas, vous devrez l'importer explicitement avec l'instruction `use`. L'utilisation de la bibliothèque `std::io` vous apporte de nombreuses fonctionnalités utiles, comme ici la possibilité de récupérer une saisie utilisateur.

