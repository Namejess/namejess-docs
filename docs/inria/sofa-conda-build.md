# Create a functionnal package of SOFA Framework to a Conda environnement

- Overview & objectives
- Solutions & tools
- PoC

## Overview

Hugo Talbot gave me a mission : I have 2 weeks to purpose a PoC of a Sofa packaging to a Conda environment.

There are some difficulties for some people to implement Sofa in a Python ecosystem (Anaconda) and there is no functionnal package at the moment. My goal is to determine what it is possible to achieve, to understand the needs of the end-users, and with which tools.

## Solutions & tools

After some discussions, there are several tools that can be used to make the packaging :

- [Pypi](https://duckduckgo.com)
- [Conda Forge / Build](https://duckduckgo.com)

Some differences against thses two tools :

### Pypi

Pip is a package manager to install python packages from Pypi. Generally, you download **source code** that is installed according to instructions in _setup.py_, using setuptools.

Most appropriate for pure Python packages, and Pip depends on a python interpreter.

### Conda

Packages are downloaded from [Anaconda Cloud](https://duckduckgo.com). With conda, we download and install a **pre-compiled platform dependent binaries**. Conda is a package manager that handles not only Python packages but also packages in other languages, and can create **virtual environments**, that do not need to contain Python.

In this case, Conda seems to be more appropriate, because of pre-compilated environments.

## PoC

Here all ressources i used to build the solution :

- [Conda Build Documentation](https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html)
- [Anaconda.org](https://anaconda.org/namejess/conda_gc_test)
- [Anaconda Documentation](https://docs.anaconda.com/anaconda/install/linux/)
- [SOFA Documentation](https://www.sofa-framework.org/community/doc/getting-started/build/windows/)
- [Conda Forge Documentation](https://conda-forge.org/docs/maintainer/knowledge_base.html#using-cmake)
- [Stack Overflow Thread](https://stackoverflow.com/questions/69185315/how-to-install-c-program-into-conda-environment-from-source)

At first I was on a Windows system but after several builds attempts (which I will come back to) I decided to go on an Archlinux system, which solved many of my problems.

To create a Conda package, you have to install the whole [Anaconda](https://docs.anaconda.com/anaconda/install/linux/) ecosystem. To use GUI packages with Linux, you will need to install the following extended dependencies for Qt:

Arch :

```shell
sudo pacman -Sy libxau libxi libxss libxtst libxcursor libxcomposite libxdamage libxfixes libxrandr libxrender mesa-libgl  alsa-lib libglvnd
```

Debian / Ubuntu :

```shell
sudo apt-get install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
```

Then, follow the [Installation Guide](https://docs.anaconda.com/anaconda/install/linux/).

To [Build a conda package from scratch](https://docs.conda.io/projects/conda-build/en/stable/user-guide/tutorials/build-pkgs.html) you have to create a file a root directory named `meta.yaml`. It is with this file that all the configuration of the package is done. Setup a directory like this :

```shell
git clone -b v22.12 https://github.com/sofa-framework/sofa.git sofa/src
```

![Tree](images/20230214100807.png)

Create a folder that will contain the `meta.yaml` file (_recipe_ is a good practice name) :

```shell
mkdir recipe
cd recipe/
touch meta.yaml
```

The template by default looks like this :

```yaml
package:
  name:
  version:

source:
  git_rev:
  git_url:

requirements:
  build:
    - python
    - setuptools

  run:
    - python

test:
  imports:
    -

about:
  home:
```

Here is the meta.yaml file I used after long trials and iterations (with commentary):

```yaml
# package name and version

package:
  name: sofa
  version: 22.11.00

# source at src/ so ..
# source url can also be used with Github url

source:
  path: ..

# All requirements for SOFA

requirements:
  build:
    - { { compiler('cxx') } }
    - "cmake"
    - "ninja"
    - "clang"
    - setuptools
    - "boost >=1.65.1"
    - "python >=3.7"
    - "qt >=5.12.9"
    - "Scipy >=1.2.0"
    - "numpy >=1.16.0"
    - "pybind11"
    - boost-cpp>=1.67

  run:
    - "ninja"
    - "clang"
    - "cmake"
    - "eigen >=3.2.10"
    - "qt >=5.12.9"
    - "qt-webengine >=5.15"
    - qt-main >=5.15
    - "python >=3.7"
    - "boost >=1.65.1"
    - boost-cpp>=1.67

  host:
    - "boost-cpp>=1.67"
    - "boost >=1.65.1"

build:
  number: 0
  rpaths:
    - lib/
    - lib/R/lib/

  # I let script in metal.yaml but you can create a build.sh file beside meta.yaml
  # This script passes the required environment variables, with Ninja/Clang
  # After we move to $SRC_DIR to launch the compilation

  script: |
    cmake -S . -B build \
    -DCMAKE_GENERATOR=Ninja \
    -DCMAKE_INSTALL_PREFIX=$PREFIX \
    -DCMAKE_CXX_COMPILER=clang++ \
    -DCMAKE_C_COMPILER=clang \
    -DPLUGIN_SOFAPYTHON3=ON \
    -DPLUGIN_SOFAMATRIX=ON 

    cd $SRC_DIR/build 
    ninja

# Extra options, look documentation for more details

about:
  home: https://github.com/sofa-framework/sofa
  license: MIT
  license_file: LICENSE

extra:
  channels:
    - conda-forge
    - defaults
```

To launch build :

```shell
sudo conda build .
```

Correct any dependency errors that may exist so that everything runs.
