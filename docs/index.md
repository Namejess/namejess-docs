# Bienvenue sur ma documentation !

## Introduction 

Ceci est une documentation de test en utilisant MkDocs. J'ai l'intention d'utiliser ce site pour mettre à jour certaines informations sur mes projets ainsi que des méthodes pour les installer et les utiliser.

## Mode sombre/clair 

N'oubliez pas que vous pouvez changer de mode sombre/clair en cliquant sur le bouton en haut à droite de la page !

## Philosophie 

Dans ces pages, vous trouverez un aperçu des différents cours et apprentissages que je suis dans mon cursus de **Concepteur Développeur d'Applications** au sein de la CCI de Laxou.

De plus, j'y déposerai mes notes personnelles sur des sujets que je souhaite approfondir ou que je souhaite partager avec vous. Ces notes sont prises à l'aide du logiciel Obsidian.

J'espère que cette documentation vous sera utile et que vous trouverez les informations dont vous avez besoin. N'hésitez pas à me contacter si vous avez des questions ou des suggestions.