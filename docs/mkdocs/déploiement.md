Le déploiement d'un projet MkDocs sur GitLab Pages est un processus simple qui peut être accompli en suivant les étapes suivantes :

- Assurez-vous d'avoir un compte GitLab et d'avoir créé un dépôt pour votre projet MkDocs.

- Créez un fichier **[.gitlab-ci.yml]** dans le répertoire racine de votre projet.

- Ajoutez le contenu suivant au fichier **[.gitlab-ci.yml]** :

```yaml
image: python:3.8-buster

pages:
  stage: deploy
  script:
    - pip install mkdocs
    - pip install mkdocs-material
    - mkdocs build --clean --site-dir public
  artifacts:
    paths:
      - public
  only:
    - main
```

*PS : ce fichier permet de définir les étapes à suivre pour le déploiement de votre projet MkDocs sur GitLab Pages. Il est possible de modifier le fichier **[.gitlab-ci.yml]** pour définir d'autres étapes, comme par exemple l'utilisation de plugins pour MkDocs.*


- Ajoutez, commit et poussez les fichiers de votre projet vers votre dépôt GitLab, en incluant les fichiers générés dans le répertoire site/ :

```bash
git add .
git commit -m "Deploiement sur GitLab Pages"
git push
```


- Vous pouvez vérifier que le déploiement se déroule correctement en allant sur la page Pipeline de GitLab.

- Vous pouvez maintenant accéder à votre documentation en visitant l'URL de votre projet GitLab Pages.

Félicitations ! 