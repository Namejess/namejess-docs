Pour installer MkDocs, vous devrez d'abord vous assurer que vous avez installé **Python** sur votre ordinateur. Vous pouvez vérifier si vous avez Python en ouvrant une invite de commande ou un terminal et en tapant python --version. Si Python est installé, cela devrait afficher la version actuelle de Python.

Si vous n'avez pas Python installé, vous pouvez le télécharger à partir du site Web officiel de Python en suivant ce lien: [https://www.python.org/downloads/](https:external.ink?to=//www.python.org/downloads/). Une fois Python installé, vous devriez être en mesure d'utiliser l'outil pip pour installer MkDocs.

Assurez-vous d'avoir pip à jour avec la commande suivante : 

```powershell
pip install -U pip
```

Pour installer MkDocs, ouvrez une invite de commande ou un terminal et tapez la commande suivante:

```powershell
pip install mkdocs
```

Cela devrait télécharger et installer MkDocs sur votre ordinateur. Vous pouvez vérifier si l'installation a réussi en tapant la commande 

```powershell
mkdocs --version
```

Cela devrait afficher la version actuelle de MkDocs que vous avez installée.

Une fois que vous avez installé MkDocs, vous pouvez créer un nouveau projet en utilisant la commande 

```powershell
mkdocs new [nom_du_projet]
```

Cela créera un dossier avec le nom que vous avez spécifié pour votre projet, avec tous les fichiers nécessaires pour utiliser MkDocs.

Ouvrez le dossier dans un éditeur de code (ici VScode) :

![imageMkdocs](images/imageMkdocsYml.png)