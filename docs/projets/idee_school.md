# Quelques idées plus ou moins structurés 

Comment faire pour que les élèves puissent apprendre à coder en retirant un maximum de frein à l'apprentissage ? 
La programmation étant une discipline complexe il faut du temps ET de la patience pour apprendre et progresser. D'autant plus dans le système plus ou moins "classique" où l'on apprend à coder en formation. 

Voici un workflow basique de comment je vois l'apprentissage de la programmation actuellement dans le centre de formation où je suis : 

![Image6](images/Screenshot_2.png)

Sur le principe je suis d'accord avec ce workflow, mais je pense qu'il y a des choses à améliorer, notamment sur la partie juste après "Théorique".
On apprend des concepts abstraits. Même avec des exercices ou des projets on peut avoir du mal à véritablement comprendre le concept. Il faut un **lubrifiant didactique** pour que les concepts soient plus facilement assimilés.

# Flux de compréhension

Voici quelques idées :

- Format "Jeux vidéos". Il existe de nombreux jeux vidéos qui permettent d'apprendre à programmer. Pensez aux jeux du studio indépendant Zactronics (Exapunks, Shenzen I/O, etc...). Pensez à 7 Billions humans. Ils permettent de comprendre des concepts de programmation.
- Format "Scratch". Scratch est un langage de programmation visuel. Il permet de créer des programmes en glissant-déposant des blocs. Il permet de comprendre des concepts de programmation.

Imaginons que l'on apprend un concept théorique, comme disons les Designs Patterns. On pourrait ensuite faire un exercice ou un projet en utilisant ce concept. Mais on pourrait aussi réaliser un *approfondissement* via un jeu vidéo ou un programme type Scratch pour comprendre en profondeur le concept.

# Visuel

Une chaine Youtube que j'adore pour le design et les visuels qui sont utilisés pour expliquer les concepts (principalement POO) :

- [CodeAesthetic](https://www.youtube.com/watch?v=hxGOiiR9ZKg)
[![](https://cdn.discordapp.com/attachments/830009390089764887/1062840053744799915/image.png)](https://www.youtube.com/watch?v=rQlMtztiAoA&t=58s)