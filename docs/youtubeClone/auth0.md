
# Auth0

#### Mise en place de Auth0

>[Auth0](https://auth0.com/)

![Image1](images/20221221164801.png)

- Le client Angular demande à Auth0 un nouveau Token
- Ce token est envoyé à l'API REST Spring Boot
- Spring valide le token vis-à-vis de Auth0

##### Création application Angular (Auth0)

![Image2](images/20221221165423.png)

- Choisir Angular en Framework
- Settings
- Ajouter http://localhost:4200 dans les URIs suivants :
	- Allowed Callback URLs
	- Allowed Logout URLs
	- Allowed Web Origins

##### Création API Spring (Auth0)

![[Pasted image 20221221170501.png]]
![Image3](images/20221221170501.png)

![[Pasted image 20221221170601.png]]
![Image4](images/20221221170601.png)

>[NPM OIDC](https://www.npmjs.com/package/angular-auth-oidc-client)
>[Github OIDC](https://github.com/damienbod/angular-auth-oidc-client)

![[Pasted image 20221221171647.png]]
![Image5](images/20221221171647.png)

![[Pasted image 20221221171741.png]]
![Image6](images/20221221171741.png)

#### Spring Auth0 Tutorial

[![Hi](images/spring_auth0.png)](https://www.youtube.com/watch?v=3BUm0ZlzBLk&list=PLSVW22jAG8pAXU0th247M7xPCekzeNdrH "Spring Atuh0 Tutorial")
