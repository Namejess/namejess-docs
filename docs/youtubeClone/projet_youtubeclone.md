### Material Angular

>https://material.angular.io/components/button/api

#### Pour les vidéos :

>https://www.npmjs.com/package/@videogular/ngx-videogular

>https://videogular.github.io/ngx-videogular-showroom/#/simple-player


#### Ajout de la méthode uploadThumbnail

```javascript
  onFileSelected($event: Event) {

    this.selectedFile = ($event.target as HTMLInputElement).files![0];

    this.selectedFileName = this.selectedFile.name;

    this.fileSelected = true;

  }

  

  onUpload() {

    this.videoService

      .uploadThumbail(this.selectedFile, this.videoId)

      .subscribe((data) => {

        //Show success message

        this.matSnackBar.open('Thumbnail uploaded successfully', 'OK');

        //show error message snack bar

      });

  }
```

#### HTML ThumbNail
Dans lequel les (click) & (change) sont bien ajoutés

```html
<div>

        <label for="inputGroupFile01"

          >Upload Thumbnail:

          <div class="custom-file">

            <input

              type="file"

              class="custom-file-input"

              id="inputGroupFile01"

              aria-describedby="inputGroupFileAddon01"

              (change)="onFileSelected($event)"

            />

          </div>

          <br />

          <div *ngIf="fileSelected">

            <div>

              <p>Selected File: {{ selectedFileName }}</p>

            </div>

            <div>

              <button

                type="submit"

                mat-raised-button

                color="primary"

                (click)="onUpload()"

              >

                Upload

              </button>

            </div>

            <br />

          </div>

        </label>

      </div>
```

#### Ajout snakcbar

#### Ajout ngxVideoPlayer 

On crée un nouveau composant Angular :

```shell
ng g c video-player
```


> [Documentation](https://videogular.github.io/ngx-videogular/docs/)

> [NPM](https://www.npmjs.com/package/@videogular/ngx-videogular)


```html
<vg-player>

    <vg-overlay-play></vg-overlay-play>

    <vg-buffering></vg-buffering>

  

    <vg-controls>

        <vg-play-pause></vg-play-pause>

        <vg-playback-button></vg-playback-button>

  

        <vg-time-display vgProperty="current" vgFormat="mm:ss"></vg-time-display>

  

        <vg-scrub-bar>

            <vg-scrub-bar-current-time></vg-scrub-bar-current-time>

            <vg-scrub-bar-buffering-time></vg-scrub-bar-buffering-time>

        </vg-scrub-bar>

  

        <vg-time-display vgProperty="left" vgFormat="mm:ss"></vg-time-display>

        <vg-time-display vgProperty="total" vgFormat="mm:ss"></vg-time-display>

  

        <vg-track-selector></vg-track-selector>

        <vg-mute></vg-mute>

        <vg-volume></vg-volume>

  

        <vg-fullscreen></vg-fullscreen>

    </vg-controls>

  

    <video [vgMedia]="$any(media)" #media id="singleVideo" preload="auto">

        <source src={{videoUrl}} type="video/mp4" > //retirer le cross origin ici

    </video>

</vg-player>
```



#### Methode saveVideo()

Pour envoyer à notre backend/MongoDB les metadatas :

```javascript
  saveVideo() {

    //Call the video service to make a http call to our backend

    const videoMetaData: VideoDTO = {
      id: this.videoId,
      title: this.saveVideoDetailsForm.get('title')?.value,
      description: this.saveVideoDetailsForm.get('description')?.value,
      tags: this.tags,
      videoStatus: this.saveVideoDetailsForm.get('videoStatus')?.value,
      videoUrl: this.videoUrl,
      thumbnailUrl: this.thumbnailUrl,
    };

    this.videoService.saveVideo(videoMetaData).subscribe((data) => {

      //Show success message

      this.matSnackBar.open('Video Metadata Updated successfully', 'OK');

    });

  }
```

```html
<div>

        <button type="submit" mat-raised-button color="primary" (click)="saveVideo()">Save</button>

      </div>
```


