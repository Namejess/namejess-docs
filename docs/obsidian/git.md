# Voici quelques commandes que j'utilise régulièrement :

```bash
git switch -C branch
```

```bash
git branch --no-merged
```

```bash
git switch -D
```

```bash
git lg --all
```

```bash
git config --global -e

[alias]
	lg = log --oneline --stat --graph
```

```bash
git branch --no-merged
```


# Stash

![GitStash](images/git-stash-basic-1.png)

```python
git stash push -m "Stash info"
git stash list
git stash pop (revenir à une stash)
git stash drop 0 
```

```sh
git remote prune origin
git remote update
```

>[Gitmoji](https://gitmoji.dev/)

>[CheatSheet](https://education.github.com/git-cheat-sheet-education.pdf)

