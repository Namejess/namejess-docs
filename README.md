[![License](https://img.shields.io/badge/License-MIT-blue)](#license)
[![Language - Markdown](https://img.shields.io/badge/Language-Markdown-blue)](https://www.markdownguide.org/)
[![Outil - MkDocs](https://img.shields.io/badge/Outil-MkDocs-red)](https://www.markdownguide.org/)


# Bienvenue dans la documentation de votre projet

Cette documentation a été créée avec [MkDocs](https://www.mkdocs.org/) et hébergée sur [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

L'objectif est de mettre et ligne et partager mes travaux et notes par rapport à ma formation Bac +3 à la CCI (Concepteur Développeur d'Applications).


## Contribuer

Si vous souhaitez contribuer à ce projet, veuillez suivre les instructions décrites dans notre [guide de contribution](CONTRIBUTING.md).

## License

Released under [MIT](/LICENSE) by [@Namejess](https://github.com/Namejess).
